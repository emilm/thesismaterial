# Quantifying the Effects of Congruence Classes on Phylodynamic Epidemiology
#### Applied Mathematics Master's Thesis Project
#### Emil Majdandzic
#### Supervised by Dr. Timothy Vaughan. Computational Biology Group, ETH Zürich, Switzerland.

## Contents

This repository contains the files (RScripts and XMLs) used to produce the results of the master's thesis. The main goal of the thesis was to describe the magnitude of the impact that congruence classes (discribed by [Louca et al.](https://doi.org/10.1093/molbev/msab149)) have on phylodynamic inference using birth-death-sampling models such as [BDSKY](https://www.pnas.org/doi/abs/10.1073/pnas.1207965110) and the [BEAST2](https://doi.org/10.1371/journal.pcbi.1006650) software.

### Practical approach

The folder [practical approach](/practical%20approach) contains the RScript [Simulation study.R](/practical%20approach/Simulation%20study.R) as well as the two respective XMLs for the BEAST2 inference used in chapter 4 of the thesis. The description of how to use the xmls is contained in the above mentioned RScript. As the name suggests, "Simulation study.R" contains a simulation study that compares inferences with trees using piecewise constant as well as continuous rate functions.

### Reproduction and likellihood computation

The folder [reproduction and likelihood computation](/reproduction%20and%20likelihood%20computation) contains the two RScripts [Analysis.R](/reproduction%20and%20likelihood%20computation/Analysis.R) and [Analysis pw.R](/reproduction%20and%20likelihood%20computation/Analysis%20pw.R) as well as the four XMLs used for the respective BEAST2 inference which are used in chapter 2 of the thesis. These scripts reproduce the results by [Louca et al.](https://doi.org/10.1093/molbev/msab149) using continuous (Analysis.R) as well as piecewise constant (Analysis pw.R) rates for the construction of the trees used in the inferences.

### Theoretical approach

The folder [theoretical approach](/theoretical%20approach) contains the RScript [Congruence class.R](/theoretical%20approach/Congruence%20class.R) used in chapter 3 of the thesis. This script constructs a congruence class and then has a closer look at the likelihood surface of the models contained in the class.

### Validation of tree simulator

The folder [validation of tree simulator](/validation%20of%20tree%20simulator) contains the RScript [Comparison BDMM Castor.R](/validation%20of%20tree%20simulator/Comparison%20BDMM%20Castor.R) as well as the respective XML file used for the BEAST2 inference. This script compares the tree simulators from the BEAST2 package [BDMM](https://doi.org/10.1093/molbev/msw064) and the R package [Castor](https://doi.org/10.1093%2Fbioinformatics%2Fbtx701) using various statistics.